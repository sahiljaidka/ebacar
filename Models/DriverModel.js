/**
 * Created by SONY on 5/21/2016.
 */

const mongoose = require('mongoose');
const Config = require('../Config');
const Schema = mongoose.Schema;
const device = Config.AppConstants.DATABASE.DEVICETYPE;
const userType = Config.AppConstants.DATABASE.USER_TYPE;

const DriverModel = new Schema({
    firstName: {type: String, trim: true, default: null, required: true},
    lastName: {type: String, trim: true, default: null, required: true},
    email: {type: String, trim: true, default: null, required: true},
    password: {type: String, default: null},
    countryCode: {type: String, required: true},
    mobileNumber: {type: String, required: true},
    gender: {type: String, default: null},
    address: {type: String, default: null},
    city: {type: String, default: null},
    state: {type: String, default: null},
    primaryZipCode: {type: String, default: null},
    additionalZipCodes: {type: Array, default: []},
    primaryHomeZone: {type: String, default: null},
    secondaryHomeZone: {type: String, default: null},
    companyCode: {type: String, default: null},

    licenseURL: {type: String, default: null},
    paperworkURL: {type: String, default: null},
    cityPermitURL: {type: String, default: null},
    insuranceProofURL: {type: String, default: null},
    isOnline: {type: Boolean, default: false},


    deviceToken: {type: String, trim: true},
    accessToken: {type: String, trim: true},
    deviceType: {
        type: String, enum: [
            device.ANDROID,
            device.IOS
        ]
    },
    OTPCode: {type: String, trim: true},
    isPhoneVerified: {type: Boolean, default: false},
    adminVerification: {type: Boolean, default: false},
    isBlocked: {type: Boolean, default: false},
    registrationDateUTC: {type: Date, default: Date.now},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    firstTimeLogin: {type: Boolean, default: false},
    userType: {type: String, default: userType.DRIVER, trim: true},
    registrationStep: {type: Number, default: 0}
});

DriverModel.index({firstName: "text"});
module.exports = mongoose.model('DriverModel', DriverModel);