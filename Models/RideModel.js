/**
 * Created by SONY on 5/21/2016.
 */

const mongoose = require('mongoose');
const Config = require('../Config');
const Schema = mongoose.Schema;

const RideModel = new Schema({
    customerId: {type: Schema.ObjectId, ref: 'CustomerModel', required: true},
    driverId: {type: Schema.ObjectId, ref: 'DriverModel', required: true},

    rideType: {type: String, default: null, trim: true},
    pickUpAddress: {type: String, default: null},
    dropOffAddress: {type: String, default: null},
    cardId: {type: String, default: null}, //TODO: need to be changed after making card API
    vehicleType: {type: String, default: null},
    rideStatus: {type: String, default: null},
    rideRequestTime: {type: Date, default: Date.now},
    passengerCount: {type: Number, default: 0},
    largeBagsCount: {type: Number, default: 0},
    scheduleTime: {type: Date, default: Date.now},
    specialInst: {type: String, default: null},
    isRideReturn: {type: Boolean, default: false},
    rideStartTime: {type: Date, default: null},
    rideEndTime: {type: Date, default: null}
});

module.exports = mongoose.model('RideModel', RideModel);