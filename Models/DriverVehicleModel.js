/**
 * Created by SONY on 5/21/2016.
 */

const mongoose = require('mongoose');
const Config = require('../Config');
const Schema = mongoose.Schema;

const DriverVehicleModel = new Schema({
    driverId: {type: Schema.ObjectId, ref: 'DriverModel', required: true},
    yearOfPurchase: {type: Number, default: 0, trim: true},
    vehicleMake: {type: String, default: null, trim: true},
    doorCount: {type: Number, default: 0},
    carColor: {type: String, default: null},
    seatCount: {type: Number, default: 0},
    stateRegTag: {type: Boolean, default: false},
    smokingFlag: {type: Boolean, default: false},
    vehicleAddedAt: {type: Date, default: Date.now},
    licensePlateURL: {type: String, default: null},
    extPhotoURL: {type: String, default: null},
    intPic1URL: {type: String, default: null},
    intPic2URL: {type: String, default: null}


});

module.exports = mongoose.model('DriverVehicleModel', DriverVehicleModel);