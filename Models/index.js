module.exports = {
    CustomerModel: require('./CustomerModel'),
    RideModel: require('./RideModel'),
    DriverModel: require('./DriverModel'),
    DriverVehicleModel: require('./DriverVehicleModel')
};