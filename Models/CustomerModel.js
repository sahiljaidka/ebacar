/**
 * Created by SONY on 5/21/2016.
 */

const mongoose = require('mongoose');
const Config = require('../Config');
const Schema = mongoose.Schema;
const device = Config.AppConstants.DATABASE.DEVICETYPE;
const userType = Config.AppConstants.DATABASE.USER_TYPE;

const CustomerModel = new Schema({
    firstName: {type: String, trim: true, default: null, required: true},
    lastName: {type: String, trim: true, default: null, required: true},
    email: {type: String, trim: true, default: null, required: true},
    password: {type: String, default: null},
    countryCode: {type: String, required: true},
    mobileNumber: {type: String, required: true},
    deviceToken: {type: String, trim: true},
    accessToken: {type: String, trim: true},
    deviceType: {
        type: String, enum: [
            device.ANDROID,
            device.IOS
        ]
    },
    gender: {
        type: String, default: null
    },
    OTPCode: {type: String, trim: true},
    isPhoneVerified: {type: Boolean, default: false},
    registrationDateUTC: {type: Date, default: Date.now},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    firstTimeLogin: {type: Boolean, default: false},
    referralCode: {type: String, default: null},
    isBlocked: {type: Boolean, default: false},
    isBusinessType: {type: Boolean, default: false},
    facebookId: {type: String, default: null},
    passwordResetToken: {type: String, default: null},
    companyName: {type: String, default: null},
    employeeId: {type: String, default: null},
    division: {type: String, default: null},
    userType: {type: String, default: userType.CUSTOMER, trim: true}
    //preferredDrivers: {type: [Schema.ObjectId], default: [], ref: 'DriverModel'}
});

CustomerModel.index({firstName: "text"});
module.exports = mongoose.model('CustomerModel', CustomerModel);