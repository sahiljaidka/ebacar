/**
 * Created by SONY on 3/24/2016.
 */
module.exports = {
    CustomerService: require('./CustomerService'),
    RideService: require('./RideService'),
    DriverService: require('./DriverService'),
    DriverVehicleService: require('./DriverVehicleService')
};