/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const Models = require('../Models');

const updateDriver = (criteria, dataToSet, options, callback) => {
    options.lean = true;
    options.new = true;
    Models.DriverModel.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
const createDriver = (objToSave, callback) => {
    new Models.DriverModel(objToSave).save(callback)
};
//Delete User in DB
const deleteDriver = (criteria, callback)=> {
    Models.DriverModel.remove(criteria, callback);
};

//Get Users from DB
const getDriver = (criteria, projection, options, callback) => {
    options.lean = true;
    Models.DriverModel.find(criteria, projection, options, callback);
};

//Get All Generated Codes from DB
const getAllGeneratedCodes = (callback) => {
    let criteria = {
        OTPCode: {$ne: null}
    };
    let projection = {
        OTPCode: 1
    };
    let options = {
        lean: true
    };
    Models.DriverModel.find(criteria, projection, options, (err, dataAry) => {
        if (err) {
            callback(err)
        } else {
            let generatedCodes = [];
            if (dataAry && dataAry.length > 0) {
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null, generatedCodes);
        }
    })
};

module.exports = {
    updateDriver: updateDriver,
    createDriver: createDriver,
    deleteDriver: deleteDriver,
    getDriver: getDriver,
    getAllGeneratedCodes: getAllGeneratedCodes
};
