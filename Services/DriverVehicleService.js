/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const Models = require('../Models');

const updateVehicle = (criteria, dataToSet, options, callback) => {
    options.lean = true;
    options.new = true;
    Models.DriverVehicleModel.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
const createVehicle = (objToSave, callback) => {
    new Models.DriverVehicleModel(objToSave).save(callback)
};
//Delete User in DB
const deleteVehicle = (criteria, callback)=> {
    Models.DriverVehicleModel.remove(criteria, callback);
};

//Get Users from DB
const getVehicle = (criteria, projection, options, callback) => {
    options.lean = true;
    Models.DriverVehicleModel.find(criteria, projection, options, callback);
};


module.exports = {
    updateVehicle: updateVehicle,
    createVehicle: createVehicle,
    deleteVehicle: deleteVehicle,
    getVehicle: getVehicle
};
