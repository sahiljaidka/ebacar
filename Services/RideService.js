/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const Models = require('../Models');

const updateRide = (criteria, dataToSet, options, callback) => {
    options.lean = true;
    options.new = true;
    Models.RideModel.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
const createRide = (objToSave, callback) => {
    new Models.RideModel(objToSave).save(callback)
};
//Delete User in DB
const deleteRide = (criteria, callback)=> {
    Models.RideModel.remove(criteria, callback);
};

//Get Users from DB
const getRide = (criteria, projection, options, callback) => {
    options.lean = true;
    Models.RideModel.find(criteria, projection, options, callback);
};


module.exports = {
    updateRide: updateRide,
    createRide: createRide,
    deleteRide: deleteRide,
    getRide: getRide
};
