/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const Models = require('../Models');

const updateCustomer = (criteria, dataToSet, options, callback) => {
    options.lean = true;
    options.new = true;
    Models.CustomerModel.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
const createCustomer = (objToSave, callback) => {
    new Models.CustomerModel(objToSave).save(callback)
};
//Delete User in DB
const deleteCustomer = (criteria, callback)=> {
    Models.CustomerModel.remove(criteria, callback);
};

//Get Users from DB
const getCustomer = (criteria, projection, options, callback) => {
    options.lean = true;
    Models.CustomerModel.find(criteria, projection, options, callback);
};

//Get All Generated Codes from DB
const getAllGeneratedCodes = (callback) => {
    let criteria = {
        OTPCode: {$ne: null}
    };
    let projection = {
        OTPCode: 1
    };
    let options = {
        lean: true
    };
    Models.CustomerModel.find(criteria, projection, options, (err, dataAry) => {
        if (err) {
            callback(err)
        } else {
            let generatedCodes = [];
            if (dataAry && dataAry.length > 0) {
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null, generatedCodes);
        }
    })
};

module.exports = {
    updateCustomer: updateCustomer,
    createCustomer: createCustomer,
    deleteCustomer: deleteCustomer,
    getCustomer: getCustomer,
    getAllGeneratedCodes: getAllGeneratedCodes
};
