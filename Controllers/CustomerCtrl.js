/**
 * Created by SONY on 5/29/2016.
 */
'use strict';
const async = require('async');
const mongoose = require('mongoose');
const Models = require('../Models');
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const createToken = require('../Utils/Token');
const Services = require('../Services');
const ERROR = Config.AppConstants.STATUS_MSG.ERROR;
const CodeGenerator = require('../Lib/CodeGenerator');
const UploadManager = require('../Lib/UploadManager');
const userType = Config.AppConstants.DATABASE.USER_TYPE;

const customerRegister = (payload, callb) => {
    let customerDoesNotExist = false;
    let accessToken = null;
    let uniqueCode = null;
    let dataToSave = payload;
    let customerData = null;
    let dataToUpdate = {};
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    payload.email = payload.email.toLowerCase();

    if (dataToSave.companyName)
        dataToSave.isBusinessType = true;

    if (!dataToSave.gender)
        dataToSave.gender = null;
    /*   if (payload.profilePic && payload.profilePic.filename) {
     dataToUpdate.profilePicURL = {
     original: null,
     thumbnail: null
     }
     }*/
    async.series([
        (cb)=> {
            if (dataToSave.facebookId) {
                if (dataToSave.password) {
                    cb(ERROR.FACEBOOK_ID_PASSWORD_ERROR);
                } else {
                    cb();
                }
            } else if (!dataToSave.password) {
                cb(ERROR.PASSWORD_REQUIRED);
            } else {
                cb();
            }
        },
        (cb)=> {
            //verify email address format
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        (cb) => {
            //Validate countryCode
            if (dataToSave.countryCode.lastIndexOf('+') == 0) {
                if (!isFinite(dataToSave.countryCode.substr(1))) {
                    cb(Config.AppConstants.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                } else {
                    cb();
                }
            } else {
                cb(Config.AppConstants.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
            }
        },
        (cb) => {
            //Validate phone No
            if (dataToSave.mobileNumber && dataToSave.mobileNumber.split('')[0] == 0) {
                cb(Config.AppConstants.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        (cb) => {
            var criteria = {
                $or: [{
                    email: payload.email
                }, {
                    mobileNumber: payload.mobileNumber
                }]
            };
            Services.CustomerService.getCustomer(criteria, {}, {
                lean: true
            }, function (err, resp) {
                if (err) {
                    cb(err)
                } else {
                    if (resp && resp.length != 0) {
                        cb(ERROR.USER_ALREADY_REGISTERED)
                    } else {
                        customerDoesNotExist = true;
                        cb()
                    }
                }
            })
        },
        (cb) => {
            CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                if (err) {
                    cb(err);
                } else {
                    if (!numberObj || numberObj.number == null) {
                        cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                    } else {
                        uniqueCode = numberObj.number;
                        cb();
                    }
                }
            })
        },
        function (cb) {
            //Insert Into DB
            dataToSave.firstTimeLogin = true;
            dataToSave.OTPCode = uniqueCode;
            dataToSave.userType = userType.CUSTOMER;
            dataToSave.mobileNumber = payload.mobileNumber;
            let refCode = dataToSave.firstName.substring(0, 2) + UniversalFunctions.generateReferralCode();
            dataToSave.referralCode = refCode.toString().toUpperCase();
            dataToSave.registrationDateUTC = new Date().toISOString();
            Services.CustomerService.createCustomer(dataToSave, function (err, customerDataFromDB) {
                //Models.Customers(dataToSave).save(function (err, customerDataFromDB) {
                console.log('hello', err, customerDataFromDB);
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('customers.$mobileNo_1') > -1) {
                        cb(ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('customers.$email_1') > -1) {
                        cb(ERROR.EMAIL_ALREADY_EXIST);

                    } else {
                        cb(err)
                    }
                } else {
                    customerData = customerDataFromDB;
                    cb();
                }
            });
        },
        function (cb) {
            //Set Access
            if (customerData) {
                var tokenData = {
                    id: customerData._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR);
            }
        },
        /*        function (cb) {
         if (customerData && customerData._id && payload.profilePic && payload.profilePic.filename) {
         UploadManager.uploadFileToS3WithThumbnail(payload.profilePic, customerData._id, 'profile', function (err, uploadedInfo) {
         if (err) {
         cb(err)
         } else {
         dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && Config.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
         dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && Config.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
         cb();
         }
         })
         } else {
         cb();
         }
         },
         function (cb) {
         if (customerData && dataToUpdate && dataToUpdate.profilePicURL && dataToUpdate.profilePicURL.original) {
         var criteria = {
         _id: customerData._id
         };
         var setQuery = {
         $set: dataToUpdate
         };
         Services.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, updatedData) {
         customerData = updatedData;
         cb()
         })
         } else {
         if (customerData && customerData._id && payload.profilePic && payload.profilePic.filename && !dataToUpdate.profilePicURL.original) {
         var criteria = {
         _id: customerData._id
         };
         Services.CustomerService.deleteCustomer(criteria, function (err, updatedData) {
         cb(Config.AppConstants.STATUS_MSG.ERROR.ERROR_PROFILE_PIC_UPLOAD);
         })
         } else {
         cb();
         }
         }
         }*/
    ], function (err, data) {
        if (err) {
            callb(err)
        } else {
            var details = {
                customerDetails: customerData,
                accessToken: accessToken
            };
            return callb(null, details);
        }
    })
};

const customerLogin = (payload, callBackRoute) => {
    let userFound = false;
    let successLogin = false;
    let accessToken = null;
    let returnedData = {};
    let updatedUserDetails = null;
    async.series([
        (callback)=> {
            //verify email address
            if (!UniversalFunctions.verifyEmailFormat(payload.email)) {
                callback(ERROR.INVALID_EMAIL);
            } else {
                callback();
            }
        },
        (callback) => {
            const criteria = {
                email: payload.email
            };
            Services.CustomerService.getCustomer(criteria, {}, {new: true, lean: true}, function (err, result) {
                if (err) {
                    callback(err)
                } else {
                    userFound = result && result[0] || null;
                    returnedData = result;
                    callback();
                }
            });
        },
        (cb) => {
            //validations
            if (!userFound) {
                cb(ERROR.UNREGISTERED_EMAIL)
            } else {
                /*                if (userFound && userFound.isPhoneVerified == false) {
                 cb(ERROR.NOT_VERIFIED)
                 }*/

                if (userFound && userFound.password != UniversalFunctions.CryptData(payload.password)) {
                    cb(ERROR.INCORRECT_PASSWORD)
                } else {
                    successLogin = true;
                    cb();
                }

            }
        },
        (cb) => {
            const criteria = {
                _id: userFound._id
            };
            const setQuery = {
                deviceToken: payload.deviceToken,
                deviceType: payload.deviceType,
                firstTimeLogin: false
            };
            Services.CustomerService.updateCustomer(criteria, setQuery, {new: true, lean: true}, function (err, data) {
                updatedUserDetails = data;
                cb();
            });
        },
        (cb) => {
            if (successLogin) {
                const tokenData = {
                    id: userFound._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            //cb("Error 2")
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR);
                //cb("Error 3")
            }
        },
        (callback) => {
            const criteria = {
                email: payload.email
            };
            Services.CustomerService.getCustomer(criteria, {}, {new: true, lean: true}, function (err, result) {
                if (err) {
                    callback(err)
                } else {
                    userFound = result && result[0] || null;
                    returnedData = result;
                    callback();
                }
            });
        }
    ], (err, data) => {
        if (err) {
            callBackRoute(err)
        } else {
            const userDetails = UniversalFunctions.deleteUnnecessaryUserData(userFound);
            const details = {
                customerDetails: userDetails,
                accessToken: accessToken
            };
            callBackRoute(null, details);
        }
    })
};

const customerLogout = (userData, callbackRoute) => {
    async.series([
            (callback) => {
                const condition = {
                    _id: userData.id
                };
                const dataToUpdate = {
                    $unset: {
                        accessToken: 1,
                        deviceToken: 1
                    }
                };
                if (userData.userType == userType.CUSTOMER) {
                    Services.CustomerService.updateCustomer(condition, dataToUpdate, {}, (err, result) => {
                        if (err) {
                            callback(err);
                        } else {
                            callback();
                        }
                    });
                }
                //TODO: code if some other userType
            }
        ],
        (error, result)=> {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute();
            }
        });
};

const customerLoginViaFacebook = (payload, callBackRoute) => {
    let userFound = false;
    let successLogin = false;
    let accessToken = null;
    let returnedData = {};
    let updatedUserDetails = null;
    async.series([
        (callback) => {
            const criteria = {
                facebookId: payload.facebookId
            };
            Services.CustomerService.getCustomer(criteria, {}, {}, function (err, result) {
                if (err) {
                    callback(err)
                } else {
                    userFound = result && result[0] || null;
                    returnedData = result;
                    callback();
                }
            });
        },
        (cb) => {
            //validations
            if (!userFound) {
                cb(ERROR.FACEBOOK_ID_NOT_FOUND)
            } else {
                /*                if (userFound && userFound.isPhoneVerified == false) {
                 cb(ERROR.NOT_VERIFIED)
                 }*/
                successLogin = true;
                cb();


            }
        },
        (cb) => {
            const criteria = {
                _id: userFound._id
            };
            const setQuery = {
                deviceToken: payload.deviceToken,
                deviceType: payload.deviceType,
                firstTimeLogin: false
            };
            Services.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                updatedUserDetails = data;
                cb();
            });
        },
        (cb) => {
            if (successLogin) {
                const tokenData = {
                    id: userFound._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        if (output && output.accessToken) {
                            accessToken = output && output.accessToken;
                            cb();
                        } else {
                            cb(ERROR.IMP_ERROR)
                        }
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR);
            }

        },
        (callback) => {
            const criteria = {
                facebookId: payload.facebookId
            };
            Services.CustomerService.getCustomer(criteria, {}, {new: true, lean: true}, function (err, result) {
                if (err) {
                    callback(err)
                } else {
                    userFound = result && result[0] || null;
                    returnedData = result;
                    callback();
                }
            });
        }
    ], (err, data) => {
        if (err) {
            callBackRoute(err)
        } else {
            const userDetails = UniversalFunctions.deleteUnnecessaryUserData(userFound);
            const details = {
                customerDetails: userDetails,
                accessToken: accessToken
            };
            callBackRoute(null, details);
        }
    })
};

const verifyOTP = (payload, tokenData, callback) => {
    if (!payload || !tokenData._id) {
        callback(ERROR.IMP_ERROR)
    } else {
        //    var newNumberToVerify = queryData.countryCode + '-' + queryData.mobileNo;
        async.series([
            (cb) => {
                //Check verification code :
                if (payload.OTPCode == tokenData.OTPCode) {
                    cb();
                } else {
                    cb(ERROR.INVALID_CODE)
                }
            },
            (cb) => {
                //trying to update customer
                const criteria = {
                    _id: tokenData.id,
                    OTPCode: payload.OTPCode
                };
                const setQuery = {
                    $set: {
                        isPhoneVerified: true
                    },
                    $unset: {
                        OTPCode: 1
                    }
                };
                const options = {
                    new: true
                };

                Services.CustomerService.updateCustomer(criteria, setQuery, options, function (err, updatedData) {

                    if (err) {
                        cb(err)
                    } else {
                        if (!updatedData) {
                            cb(ERROR.INVALID_CODE)
                        } else {
                            cb();
                        }
                    }
                });
            }
        ], (err, result) => {
            if (err) {
                callback(err)
            } else {
                callback();
            }

        });
    }
};

const resendOTP = (userData, callback) => {

    const mobileNumber = userData.mobileNumber;
    const countryCode = userData.countryCode;
    let dataFound;
    if (!mobileNumber) {
        callback(ERROR.IMP_ERROR);
    } else {
        var uniqueCode = null;
        async.series([
            (cb) => {
                const query = {
                    mobileNumber: userData.mobileNumber
                };
                const projection = {
                    _id: 1,
                    mobileNumber: 1,
                    isPhoneVerified: 1,
                    countryCode: 1
                };
                Services.CustomerService.getCustomer(query, projection, {}, function (err, data) {
                    if (err) {
                        cb(ERROR.PASSWORD_CHANGE_REQUEST_INVALID);
                    } else {
                        dataFound = data && data[0] || null;
                        if (dataFound == null) {
                            cb(ERROR.PHONENUMBER_NOT_REGISTERED);
                        } else {
                            if (dataFound.isPhoneVerified == true) {
                                cb(ERROR.PHONE_VERIFICATION_COMPLETE);
                            } else {
                                cb();
                            }
                        }
                    }
                });
            },
            (cb) => {
                CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.CUSTOMER, (err, numberObj) => {
                    if (err) {
                        cb(err);
                    } else {
                        if (!numberObj || numberObj.number == null) {
                            cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
                        } else {
                            uniqueCode = numberObj.number;
                            cb();
                        }
                    }
                })
            },
            (cb) => {
                const criteria = {
                    _id: userData.id
                };
                const setQuery = {
                    $set: {
                        OTPCode: uniqueCode
                    }
                };
                const options = {
                    lean: true
                };
                Services.CustomerService.updateCustomer(criteria, setQuery, options, cb);
            }

        ], function (err, result) {
            callback(err, null);
        })
    }
};

const changePassword = function (payload, tokenData, callbackRoute) {
    let oldPassword = UniversalFunctions.CryptData(payload.oldPassword);
    let newPassword = UniversalFunctions.CryptData(payload.newPassword);
    async.series([
            (callback) => {
                const query = {
                    _id: tokenData.id
                };
                const projection = {
                    password: 1
                };
                const options = {
                    lean: true
                };
                Services.CustomerService.getCustomer(query, projection, options, function (err, data) {
                    if (err) {
                        callback(err);
                    } else {
                        let customerData = data && data[0] || null;
                        console.log("customerData-------->>>" + JSON.stringify(customerData));
                        if (customerData == null) {
                            callback(ERROR.NOT_FOUND);
                        } else {
                            if (data[0].password == oldPassword && data[0].password != newPassword) {
                                callback();
                            } else if (data[0].password != oldPassword) {
                                callback(ERROR.WRONG_PASSWORD)
                            } else if (data[0].password == newPassword) {
                                callback(ERROR.NOT_UPDATE)
                            }
                        }
                    }
                });
            },
            (callback) => {
                const dataToUpdate = {
                    $set: {
                        'password': UniversalFunctions.CryptData(payload.newPassword)
                    }
                };
                const condition = {
                    _id: tokenData.id
                };
                Services.CustomerService.updateCustomer(condition, dataToUpdate, {}, function (err, user) {
                    console.log("customerData-------->>>" + JSON.stringify(user));
                    if (err) {
                        callback(err);
                    } else {
                        if (!user || user.length == 0) {
                            callback(ERROR.NOT_FOUND);
                        } else {
                            callback(null);
                        }
                    }
                });
            }
        ],
        (error, result) => {
            if (error) {
                return callbackRoute(error);
            } else {
                return callbackRoute(null);
            }
        });
};

module.exports = {
    customerRegister: customerRegister,
    customerLogin: customerLogin,
    customerLogout: customerLogout,
    customerLoginViaFacebook: customerLoginViaFacebook,
    verifyOTP: verifyOTP,
    resendOTP: resendOTP,
    changePassword: changePassword
};