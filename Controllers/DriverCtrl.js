/**
 * Created by SONY on 5/29/2016.
 */
'use strict';
const async = require('async');
const mongoose = require('mongoose');
const Models = require('../Models');
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const createToken = require('../Utils/Token');
const Services = require('../Services');
const ERROR = Config.AppConstants.STATUS_MSG.ERROR;
const CodeGenerator = require('../Lib/CodeGenerator');
const UploadManager = require('../Lib/UploadManager');
const userType = Config.AppConstants.DATABASE.USER_TYPE;

const driverRegisterStep1 = (payload, callb) => {
    let driverDoesNotExist = false;
    let accessToken = null;
    let dataToSave = payload;
    let driverData = null;
    let dataToUpdate = {};
    if (dataToSave.password)
        dataToSave.password = UniversalFunctions.CryptData(dataToSave.password);
    payload.email = payload.email.toLowerCase();

    /*   if (payload.profilePic && payload.profilePic.filename) {
     dataToUpdate.profilePicURL = {
     original: null,
     thumbnail: null
     }
     }*/
    async.series([
        (cb)=> {
            //verify email address format
            if (!UniversalFunctions.verifyEmailFormat(dataToSave.email)) {
                cb(ERROR.INVALID_EMAIL);
            } else {
                cb();
            }
        },
        (cb) => {
            //Validate countryCode
            if (dataToSave.countryCode.lastIndexOf('+') == 0) {
                if (!isFinite(dataToSave.countryCode.substr(1))) {
                    cb(Config.AppConstants.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                } else {
                    cb();
                }
            } else {
                cb(Config.AppConstants.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
            }
        },
        (cb) => {
            //Validate phone No
            if (dataToSave.mobileNumber && dataToSave.mobileNumber.split('')[0] == 0) {
                cb(Config.AppConstants.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
            } else {
                cb();
            }
        },
        (cb) => {
            var criteria = {
                $or: [{
                    email: payload.email
                }, {
                    mobileNumber: payload.mobileNumber
                }]
            };
            Services.DriverService.getDriver(criteria, {}, {
                lean: true
            }, function (err, resp) {
                if (err) {
                    cb(err)
                } else {
                    if (resp && resp.length != 0) {
                        cb(ERROR.USER_ALREADY_REGISTERED)
                    } else {
                        driverDoesNotExist = true;
                        cb()
                    }
                }
            })
        },
        /*        (cb) => {
         CodeGenerator.generateUniqueCode(4, Config.AppConstants.DATABASE.USER_ROLES.DRIVER, function (err, numberObj) {
         if (err) {
         cb(err);
         } else {
         if (!numberObj || numberObj.number == null) {
         cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
         } else {
         uniqueCode = numberObj.number;
         cb();
         }
         }
         })
         },*/
        function (cb) {
            //Insert Into DB
            dataToSave.firstTimeLogin = true;
            dataToSave.registrationStep = 1;
            dataToSave.mobileNumber = payload.mobileNumber;
            dataToSave.registrationDateUTC = new Date().toISOString();
            Services.DriverService.createDriver(dataToSave, (err, driverDataFromDB) => {

                console.log('hello', err, driverDataFromDB);
                if (err) {
                    if (err.code == 11000 && err.message.indexOf('customers.$mobileNo_1') > -1) {
                        cb(ERROR.PHONE_NO_EXIST);

                    } else if (err.code == 11000 && err.message.indexOf('customers.$email_1') > -1) {
                        cb(ERROR.EMAIL_ALREADY_EXIST);

                    } else {
                        cb(err)
                    }
                } else {
                    driverData = driverDataFromDB;
                    cb();
                }
            });
        },
        function (cb) {
            //Set Access
            if (driverData) {
                var tokenData = {
                    id: driverData._id,
                    type: Config.AppConstants.DATABASE.USER_ROLES.DRIVER
                };
                createToken.setToken(tokenData, function (err, output) {
                    if (err) {
                        cb(err);
                    } else {
                        accessToken = output && output.accessToken || null;
                        cb();
                    }
                })
            } else {
                cb(ERROR.IMP_ERROR);
            }
        },
        /*        function (cb) {
         if (customerData && customerData._id && payload.profilePic && payload.profilePic.filename) {
         UploadManager.uploadFileToS3WithThumbnail(payload.profilePic, customerData._id, 'profile', function (err, uploadedInfo) {
         if (err) {
         cb(err)
         } else {
         dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && Config.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
         dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && Config.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
         cb();
         }
         })
         } else {
         cb();
         }
         },
         function (cb) {
         if (customerData && dataToUpdate && dataToUpdate.profilePicURL && dataToUpdate.profilePicURL.original) {
         var criteria = {
         _id: customerData._id
         };
         var setQuery = {
         $set: dataToUpdate
         };
         Services.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, updatedData) {
         customerData = updatedData;
         cb()
         })
         } else {
         if (customerData && customerData._id && payload.profilePic && payload.profilePic.filename && !dataToUpdate.profilePicURL.original) {
         var criteria = {
         _id: customerData._id
         };
         Services.CustomerService.deleteCustomer(criteria, function (err, updatedData) {
         cb(Config.AppConstants.STATUS_MSG.ERROR.ERROR_PROFILE_PIC_UPLOAD);
         })
         } else {
         cb();
         }
         }
         }*/
    ], function (err, data) {
        if (err) {
            callb(err)
        } else {
            var details = {
                driverDetails: driverData,
                accessToken: accessToken
            };
            return callb(null, details);
        }
    })
};

const driverRegisterStep2 = (payload, tokenData, callb) => {

    let dataToSave = payload;
    let driverData = null;

    async.series([

        (cb) => {
            //Update Driver details in DB
            dataToSave.registrationStep = 2;
            let criteria = {_id: tokenData._id};

            Services.DriverService.updateDriver(criteria, dataToSave, {new: true, lean: true}, (err, data)=> {
                if (err) {
                    cb(err)
                }
                else {
                    driverData = data;
                    cb();
                }
            });
        },
        (cb) => {
            var criteria = {
                _id: tokenData._id
            };
            Services.DriverService.getDriver(criteria, {}, {
                lean: true
            }, function (err, resp) {
                if (err) {
                    cb(err)
                } else {
                    if (resp && resp.length != 0) {
                        driverData = resp;
                        cb();
                    } else {

                        cb("Not found");
                    }
                }
            })
        }

    ], function (err, data) {
        if (err) {
            callb(err)
        } else {
            return callb(null, driverData);
        }
    })
};

const addVehicle = (payload, tokenData, callb) => {

    let dataToSave = payload;
    let vehicleData = null;

    async.series([
        (cb) => {
            dataToSave.driverId = tokenData._id;
            console.log("Token data.id", tokenData._id);
            Services.DriverVehicleService.createVehicle(dataToSave, (err, data)=> {
                if (err) {
                    cb(err)
                }
                else {
                    vehicleData = data;
                    cb();
                }
            });
        },
        (cb) => {
            var criteria = {
                driverId: tokenData._id
            };
            Services.DriverVehicleService.getVehicle(criteria, {}, {
                lean: true
            }, function (err, resp) {
                if (err) {
                    cb(err)
                } else {
                    if (resp && resp.length != 0) {
                        vehicleData = resp;
                        cb();
                    } else {
                        console.log("I am here");
                        cb("Not found");
                    }
                }
            })
        }
    ], function (err, data) {
        if (err) {
            callb(err)
        } else {
            return callb(null, vehicleData);
        }
    })
};

module.exports = {
    driverRegisterStep1: driverRegisterStep1,
    driverRegisterStep2: driverRegisterStep2,
    addVehicle: addVehicle
};