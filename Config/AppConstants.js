/**
 * Created by SONY on 3/12/2016.
 */
'use strict';

var SERVER = {
    AppName: 'TestHapi',
    Ports: {
        HAPI: 3030,
        host: 'localhost'
    },
    THUMB_WIDTH: 50,
    THUMB_HEIGHT: 50
};

var DATABASE = {
    USER_TYPE: {
        CUSTOMER: 'CUSTOMER',
        DRIVER: 'DRIVER'
    },
    BOOKING_STATUS: {
        NOT_ACCEPTED: 'NOT_ACCEPTED',
        WAITING: 'WAITING',
        BOOKED: 'BOOKED',
        ONGOING: 'ONGOING',
        STARTED: 'STARTED',
        COMPLETED: 'COMPLETED',
        CANCELLED: 'CANCELLED',
        DECLINED: 'DECLINED',
        EXPIRED: 'EXPIRED',
        DROPPED: 'DROPPED',
        REACHED: 'REACHED',
        PENDING: 'PENDING'
    }, FILE_TYPES: {
        LOGO: 'LOGO',
        DOCUMENT: 'DOCUMENT',
        OTHERS: 'OTHERS'
    },
    DOCUMENT_PREFIX: 'document_',
    LOGO_PREFIX: {
        ORIGINAL: 'logo_',
        THUMB: 'logoThumb_'
    },
    ROLES: {
        SUPER_ADMIN: 'SUPER_ADMIN',
        CALL_CENTER: 'CALL_CENTER',
        FACILITATOR: 'FACILITATOR',
        FINANCE: 'FINANCE',
        MARKETING_REPOS: 'MARKETING_REPOS'
    },
    BOOK_GENRE: {
        ROMANTIC: 'ROMANTIC',
        COMEDY: 'COMEDY',
        ACTION: 'ACTION',
        FICTION: 'FICTION'
    },
    GENDER: {
        MALE: 'MALE',
        FEMALE: 'FEMALE',
        NULL: 'NULL'
    },
    DEVICETYPE: {
        IOS: 'IOS',
        ANDROID: 'ANDROID'
    },
    PROFILE_PIC_PREFIX: {
        ORIGINAL: 'profilePic_',
        THUMB: 'profileThumb_'
    },
    USER_ROLES: {
        ADMIN: 'ADMIN',
        CUSTOMER: 'CUSTOMER',
        DRIVER: 'DRIVER'
    }
};

var STATUS_MSG = {
    ERROR: {
        INCORRECT_USERNAME: {
            statusCode: 401,
            customMessage: 'Username not registered with us.',
            type: 'INCORRECT_USERNAME'
        },
        //ADMIN_ALREADY_REGISTERED: {
        //    statusCode: 400,
        //    customMessage: 'Admin already registered with this username.',
        //    type: 'ADMIN_ALREADY_REGISTERED'
        //},
        CALL_CONNECT: {
            statusCode: 400,
            customMessage: 'Unable to connect.',
            type: '  CALL_CONNECT'
        },
        ALREADY_REJECTED: {
            statusCode: 400,
            customMessage: 'Already rejected booking . Now cannot accept',
            type: 'ALREADY_REJECTED'
        },
        INVALID_LAT: {
            statusCode: 400,
            customMessage: 'Invalid latitude and longitude.',
            type: 'INVALID_LAT'
        },
        THERAPIST_REJECT: {
            statusCode: 400,
            customMessage: 'You have already reject this booking.',
            type: 'THERAPIST_REJECT'
        },
        NO_THERAPIST: {
            statusCode: 400,
            customMessage: 'No therapist available.',
            type: ' NO_THERAPIST'
        },
        NO_BOOKINGS: {
            statusCode: 400,
            customMessage: 'No Bookings for you.',
            type: 'NO_BOOKINGS'
        },
        NO_RECEIVED: {
            statusCode: 400,
            customMessage: 'Not received any gift Card.',
            type: 'NO_RECEIVED'
        },
        TOKEN: {
            statusCode: 400,
            customMessage: 'Please enter valid token.',
            type: 'TOKEN'
        },
        NOT_SERVICE: {
            statusCode: 400,
            customMessage: 'Sorry!! Prices are not set at this location. You can select any other massage.',
            type: ' NOT_SERVICE'
        },
        ALREADY_DELETE: {
            statusCode: 400,
            customMessage: 'Already a deleted address',
            type: 'ALREADY_DELETE'
        },
        ALREADY_ACCEPTED: {
            statusCode: 400,
            customMessage: 'You have already accepted this booking. cannot reject now',
            type: 'ALREADY_ACCEPTED'
        },
        ALREADY_BUSY: {
            statusCode: 400,
            customMessage: 'Already Busy in this slot.',
            type: 'ALREADY_BUSY'
        },
        ALREADY_CLAIMED_JOB: {
            statusCode: 400,
            customMessage: 'Already Claimed Job.',
            type: 'ALREADY_CLAIMED_JOB'
        },
        INVALID_ACTION_ADDRESS: {
            statusCode: 400,
            customMessage: 'Address Id does not exists.',
            type: 'INVALID_ACTION'
        },
        DEFAULT_NOT_DELETE: {
            statusCode: 400,
            customMessage: 'You cannot delete default Address.',
            type: ' DEFAULT_NOT_DELETE'
        },
        NOT_LOCATION: {
            statusCode: 400,
            customMessage: 'Sorry!! We do not serve services here.',
            type: 'NOT_LOCATION'
        },
        DEFAULTCARD_NOT_DELETE: {
            statusCode: 400,
            customMessage: 'You cannot delete default card.',
            type: 'DEFAULTCARD_NOT_DELETE'
        },
        INVALID_ACTION: {
            statusCode: 400,
            customMessage: 'Invalid Action',
            type: 'INVALID_ACTION'
        },
        NOT_DELETED: {
            statusCode: 400,
            customMessage: 'Unable To Delete.',
            type: 'NOT_DELETED'
        },
        ALREADY_DELETED: {
            statusCode: 400,
            customMessage: 'Card Already Deleted.',
            type: 'ALREADY_DELETED'
        },
        SET_DEFAULT_ERROR: {
            statusCode: 400,
            customMessage: 'Unable to set this as default',
            type: 'SET_DEFAULT_ERROR'
        },
        INVALID_CARD: {
            statusCode: 400,
            customMessage: 'Please Enter valid card details.',
            type: 'INVALID_CARD'
        },
        INCORRECT_ACCESSTOKEN: {
            statusCode: 403,
            customMessage: 'Incorrect AccessToken',
            type: 'INCORRECT_ACCESSTOKEN'
        },
        EMAIL_DOMAIN_ERROR: {
            statusCode: 400,
            customMessage: 'Please enter valid Email Address',
            type: 'EMAIL_DOMAIN_ERROR'
        },
        USER_ALREADY_REGISTERED: {
            statusCode: 400,
            customMessage: 'Your Email Address or Mobile Number is already Registered.',
            type: 'USER_ALREADY_REGISTERED'
        },
        BOOK_ALREADY_EXISTS: {
            statusCode: 400,
            customMessage: 'This book already exists.',
            type: 'BOOK_ALREADY_EXISTS'
        },
        ADMIN_ALREADY_REGISTERED: {
            statusCode: 400,
            customMessage: 'This Email address is already Registered.',
            type: 'ADMIN_ALREADY_REGISTERED'
        },
        DUPLICATE_FACEBOOK_ID: {
            statusCode: 400,
            customMessage: 'You are already registered with us.',
            type: 'DUPLICATE_FACEBOOK_ID'
        },
        NOT_UPDATED: {
            statusCode: 501,
            customMessage: 'Unable To Update',
            type: 'NOT_UPDATED'
        },
        NOT_UPDATE: {
            statusCode: 401,
            customMessage: 'New password must be different from Old password.',
            type: 'NOT_UPDATE'
        },
        WRONG_PASSWORD: {
            statusCode: 400,
            customMessage: 'Incorrect Old Password.',
            type: 'WRONG_PASSWORD'
        },
        USER_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'User not found.',
            type: 'USER_NOT_FOUND'
        },
        NOT_VERIFIED: {
            statusCode: 400,
            customMessage: 'Please verify your Mobile Number.',
            type: 'NOT_VERIFIED'
        },
        BOOKING_ERROR: {
            statusCode: 400,
            customMessage: 'Please verify your account to proceed.',
            type: 'BOOKING_ERROR'
        },
        INCORRECT_ID: {
            statusCode: 401,
            customMessage: 'Incorrect User',
            type: 'INCORRECT_ID'
        },
        INCORRECT_MOBILE: {
            statusCode: 401,
            customMessage: 'Incorrect Mobile Number',
            type: 'INCORRECT_MOBILE'
        },
        PASSWORD_CHANGE_REQUEST_EXPIRE: {
            statusCode: 400,
            customMessage: ' Password change request time expired',
            type: 'PASSWORD_CHANGE_REQUEST_EXPIRE'
        },
        PASSWORD_CHANGE_REQUEST_INVALID: {
            statusCode: 400,
            type: 'PASSWORD_CHANGE_REQUEST_INVALID',
            customMessage: 'Invalid password change request.'

        },
        INVALID_USER_PASS: {
            statusCode: 401,
            type: 'INVALID_USER_PASS',
            customMessage: 'Invalid username or password'
        },
        TOKEN_ALREADY_EXPIRED: {
            statusCode: 401,
            customMessage: 'Token Already Expired',
            type: 'TOKEN_ALREADY_EXPIRED'
        },
        DB_ERROR: {
            statusCode: 400,
            customMessage: 'DB Error : ',
            type: 'DB_ERROR'
        },
        INVALID_ID: {
            statusCode: 400,
            customMessage: 'Invalid Id Provided : ',
            type: 'INVALID_ID'
        },
        PREVIOUS_BUSY: {
            statusCode: 400,
            customMessage: 'Previous Therapist is busy.',
            type: 'PREVIOUS_BUSY'

        },
        APP_ERROR: {
            statusCode: 400,
            customMessage: 'Application Error',
            type: 'APP_ERROR'
        },

        CANNOT_START: {
            statusCode: 400,
            customMessage: 'You cannot start this service now.',
            type: 'CANNOT_START'
        },
        START_TIME: {
            statusCode: 400,
            customMessage: 'Please select Time according to first therapist.',
            type: 'START_TIME'
        },
        ADDRESS_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Address not found.',
            type: 'ADDRESS_NOT_FOUND'
        },
        ADDRESS_DELETED: {
            statusCode: 400,
            customMessage: 'this address was deleted by you. Please enter valid address',
            type: ' ADDRESS_DELETED'
        },
        CARD_DELETED: {
            statusCode: 400,
            customMessage: 'this card was deleted by you. Please enter valid card',
            type: ' CARD_DELETED'
        },
        NOT_ASSIGNED: {
            statusCode: 400,
            customMessage: 'BOOKING NOT ASSIGNED TO YOU',
            type: 'NOT_ASSIGNED'
        },
        SAME_ADDRESS_ID: {
            statusCode: 400,
            customMessage: 'Massage type with same area already exists.',
            type: 'SAME_ADDRESS_ID'
        },
        PICKUP_ADDRESS_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Pickup Address not found',
            type: 'PICKUP_ADDRESS_NOT_FOUND'
        },
        DELIVERY_ADDRESS_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Delivery Address not found',
            type: 'DELIVERY_ADDRESS_NOT_FOUND'
        },
        IMP_ERROR: {
            statusCode: 500,
            customMessage: 'Implementation Error',
            type: 'IMP_ERROR'
        },
        APP_VERSION_ERROR: {
            statusCode: 400,
            customMessage: 'One of the latest version or updated version value must be present',
            type: 'APP_VERSION_ERROR'
        },
        INVALID_TOKEN: {
            statusCode: 401,
            customMessage: 'Invalid token provided',
            type: 'INVALID_TOKEN'
        },
        INVALID_CODE: {
            statusCode: 400,
            customMessage: 'Invalid OTP verification code.',
            type: 'INVALID_CODE'
        },
        DEFAULT: {
            statusCode: 400,
            customMessage: 'Error',
            type: 'DEFAULT'
        },
        DISCOUNT: {
            statusCode: 400,
            customMessage: 'Both gift Card and promo code cannot be used simultaneously',
            type: 'DISCOUNT'
        },
        PHONE_NO_EXIST: {
            statusCode: 400,
            customMessage: 'Mobile No Already Exist',
            type: 'PHONE_NO_EXIST'
        },
        PHONE_VERIFICATION: {
            statusCode: 400,
            customMessage: 'Please verify your Mobile Number.',
            type: ' PHONE_VERIFICATION'
        },
        PHONE_VERIFICATION_COMPLETE: {
            statusCode: 400,
            customMessage: 'Your mobile number verification is already completed.',
            type: 'PHONE_VERIFICATION_COMPLETE'
        },
        EMAIL_EXIST: {
            statusCode: 400,
            customMessage: 'Email Already Exist',
            type: 'EMAIL_EXIST'
        },
        DUPLICATE: {
            statusCode: 400,
            customMessage: 'Duplicate Entry',
            type: 'DUPLICATE'
        },
        DUPLICATE_ADDRESS: {
            statusCode: 400,
            customMessage: 'Address Already Exist',
            type: 'DUPLICATE_ADDRESS'
        },
        UNIQUE_CODE_LIMIT_REACHED: {
            statusCode: 400,
            customMessage: 'Cannot Generate Unique Code, All combinations are used',
            type: 'UNIQUE_CODE_LIMIT_REACHED'
        },
        INVALID_REFERRAL_CODE: {
            statusCode: 400,
            customMessage: 'Invalid Referral Code',
            type: 'INVALID_REFERRAL_CODE'
        },
        FACEBOOK_ID_PASSWORD_ERROR: {
            statusCode: 400,
            customMessage: 'Only one field should be filled at a time, either facebookId or password',
            type: 'FACEBOOK_ID_PASSWORD_ERROR'
        },
        NOT_REGISTERED_ROLE: {
            statusCode: 401,
            customMessage: 'You are not registered for this role.',
            type: 'NOT_REGISTERED_ROLE'
        },
        INVALID_EMAIL: {
            statusCode: 400,
            customMessage: 'Invalid Email Address.',
            type: 'INVALID_EMAIL'
        },
        PASSWORD_REQUIRED: {
            statusCode: 400,
            customMessage: 'Password is required',
            type: 'PASSWORD_REQUIRED'
        },
        INVALID_COUNTRY_CODE: {
            statusCode: 400,
            customMessage: 'Invalid Country Code, Should be in the format +52',
            type: 'INVALID_COUNTRY_CODE'
        },
        INVALID_PHONE_NO_FORMAT: {
            statusCode: 400,
            customMessage: 'Phone no. cannot start with 0',
            type: 'INVALID_PHONE_NO_FORMAT'
        },
        COUNTRY_CODE_MISSING: {
            statusCode: 400,
            customMessage: 'You forgot to enter the country code',
            type: 'COUNTRY_CODE_MISSING'
        },
        INVALID_PHONE_NO: {
            statusCode: 400,
            customMessage: 'Phone No. & Country Code does not match to which the OTP was sent',
            type: 'INVALID_PHONE_NO'
        },
        PHONE_NO_MISSING: {
            statusCode: 400,
            customMessage: 'You forgot to enter the phone no.',
            type: 'PHONE_NO_MISSING'
        },
        NOTHING_TO_UPDATE: {
            statusCode: 400,
            customMessage: 'Nothing to update',
            type: 'NOTHING_TO_UPDATE'
        },
        NOT_FOUND: {
            statusCode: 400,
            customMessage: 'User Not Found',
            type: 'NOT_FOUND'
        },
        INVALID_RESET_PASSWORD_TOKEN: {
            statusCode: 400,
            customMessage: 'Invalid Reset Password Token',
            type: 'INVALID_RESET_PASSWORD_TOKEN'
        },
        INCORRECT_PASSWORD: {
            statusCode: 401,
            customMessage: 'Incorrect Password.',
            type: 'INCORRECT_PASSWORD'
        },
        EMPTY_VALUE: {
            statusCode: 400,
            customMessage: 'Empty String Not Allowed',
            type: 'EMPTY_VALUE'
        },
        PHONE_NOT_MATCH: {
            statusCode: 400,
            customMessage: "Phone No. Doesn't Match",
            type: 'PHONE_NOT_MATCH'
        },
        SAME_PASSWORD: {
            statusCode: 400,
            customMessage: 'Old password and new password are same',
            type: 'SAME_PASSWORD'
        },
        ACTIVE_PREVIOUS_SESSIONS: {
            statusCode: 400,
            customMessage: 'You already have previous active sessions, confirm for flush',
            type: 'ACTIVE_PREVIOUS_SESSIONS'
        },
        EMAIL_ALREADY_EXIST: {
            statusCode: 400,
            customMessage: 'Email Address Already Exists',
            type: 'EMAIL_ALREADY_EXIST'
        },
        ERROR_PROFILE_PIC_UPLOAD: {
            statusCode: 400,
            customMessage: 'Profile pic is not a valid file',
            type: 'ERROR_PROFILE_PIC_UPLOAD'
        },
        PHONE_ALREADY_EXIST: {
            statusCode: 400,
            customMessage: 'Phone No. Already Exists',
            type: 'PHONE_ALREADY_EXIST'
        },
        EMAIL_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Email Not Found',
            type: 'EMAIL_NOT_FOUND'
        },
        UNREGISTERED_EMAIL: {
            statusCode: 400,
            customMessage: 'Please enter registered Email or Mobile Number.',
            type: 'NOT_REGISTERED_EMAIL'
        },
        REFREE_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Refree not found',
            type: 'REFREE_NOT_FOUND'
        },
        FACEBOOK_ID_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Facebook Id Not Found',
            type: 'FACEBOOK_ID_NOT_FOUND'
        },
        PHONE_NOT_FOUND: {
            statusCode: 400,
            customMessage: 'Phone No. Not Found',
            type: 'PHONE_NOT_FOUND'
        },
        INCORRECT_OLD_PASS: {
            statusCode: 400,
            customMessage: 'Incorrect Old Password',
            type: 'INCORRECT_OLD_PASS'
        },
        UNAUTHORIZED: {
            statusCode: 401,
            customMessage: 'You are not authorized to perform this action',
            type: 'UNAUTHORIZED'
        },
        PHONENUMBER_NOT_REGISTERED: {
            statusCode: 400,
            customMessage: 'Please enter your registered Mobile Number.',
            type: 'PHONENUMBER_NOT_REGISTERED'
        },

    },
    SUCCESS: {
        ADDRESS_DELETE: {
            statusCode: 200,
            customMessage: 'Address deleted successfully.',
            type: 'ADDRESS_DELETE'
        },
        ADDRESS_CREATE: {
            statusCode: 200,
            customMessage: 'Address added successfully.',
            type: 'ADDRESS_CREATE'
        },
        CARD_DELETED: {
            statusCode: 200,
            customMessage: 'Card deleted successfully',
            type: 'CARD_DELETED'
        },
        VERIFY_COMPLETE: {
            statusCode: 200,
            customMessage: 'Your Mobile Number has been verified successfully.',
            type: 'VERIFY_SENT'
        },
        PASSWORD_RESET: {
            statusCode: 200,
            customMessage: 'Password reset successfully.',
            type: 'PASSWORD_RESET'
        },
        VERIFY_SENT: {
            statusCode: 200,
            customMessage: 'One Time Password has been sent to your mobile number.',
            type: 'VERIFY_SENT'
        },
        CREATED: {
            statusCode: 201,
            customMessage: 'You are Registered Successfully.',
            type: 'CREATED'
        },
        DEFAULT: {
            statusCode: 200,
            customMessage: 'Success',
            type: 'DEFAULT'
        },
        UPDATED: {
            statusCode: 200,
            customMessage: 'Updated Successfully',
            type: 'UPDATED'
        },
        LOGOUT: {
            statusCode: 200,
            customMessage: 'Logged out successfully.',
            type: 'LOGOUT'
        },
        DELETED: {
            statusCode: 200,
            customMessage: 'Deleted Successfully',
            type: 'DELETED'
        },
        NEW_PASSWORD: {
            statusCode: 200,
            customMessage: 'New Password has been sent to your registered Mobile Number',
            type: 'NEW_PASSWORD'
        }
    }
};

var swaggerDefaultResponseMessages = [
    {code: 200, message: 'OK'},
    {code: 400, message: 'Bad Request'},
    {code: 401, message: 'Unauthorized'},
    {code: 404, message: 'Data Not Found'},
    {code: 500, message: 'Internal Server Error'}
];

var AppConstants = {
    SERVER: SERVER,
    DATABASE: DATABASE,
    STATUS_MSG: STATUS_MSG,
    swaggerDefaultResponseMessages: swaggerDefaultResponseMessages
};


module.exports = AppConstants;