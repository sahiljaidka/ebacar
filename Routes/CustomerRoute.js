/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const uuid = require('node-uuid');
const Joi = require('joi');
const Controllers = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Config = require('../Config');

const deviceType = Config.AppConstants.DATABASE.DEVICETYPE;

const customerRegister = {
    method: 'POST',
    path: '/api/v1/customer/register',
    handler: function (request, reply) {
        let payload = request.payload;
        Controllers.CustomerCtrl.customerRegister(payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));

            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.CREATED, data)).code(201);

            }
        });
    },
    config: {
        description: 'Register Customer',
        tags: ['api', 'customer'],
        /*      payload: {
         maxBytes: 2000000,
         output: 'file',
         parse: true
         },*/
        validate: {
            payload: {
                firstName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                lastName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                email: Joi.string().required().email(),
                password: Joi.string().min(6).optional().allow(''),
                countryCode: Joi.string().required(),
                mobileNumber: Joi.string().min(5).regex(/^[0-9]+$/).required(),
                deviceToken: Joi.string().required(),
                deviceType: Joi.string().required().valid([deviceType.IOS, deviceType.ANDROID]),
                gender: Joi.string().optional().allow(''),
                //gender: Joi.string(),
                referralCode: Joi.string().optional().allow(''),
                facebookId: Joi.string().optional().allow(''),
                companyName: Joi.string().optional().allow(''),
                employeeId: Joi.string().optional().allow(''),
                division: Joi.string().optional().allow('')

                /* profilePic: Joi.any()
                 .meta({swaggerType: 'file'})
                 .optional()
                 .description('image file')*/
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                //payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const customerLogin = {
    method: 'POST',
    path: '/api/v1/customer/login',
    handler: function (request, reply) {
        Controllers.CustomerCtrl.customerLogin(request.payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));

            } else {
                reply(UniversalFunctions.sendSuccess(null, data));

            }
        });
    },
    config: {
        description: 'Login for Customer',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                email: Joi.string().required(),
                password: Joi.string().required(),
                deviceToken: Joi.string().required(),
                deviceType: Joi.string().required().valid([deviceType.IOS, deviceType.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const customerLogout = {
    method: 'PUT',
    path: '/api/v1/customer/logout',
    handler: function (request, reply) {
        const userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerCtrl.customerLogout(userData, (err, data) => {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Customer Logout',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const customerLoginViaFacebook = {
    method: 'POST',
    path: '/api/v1/customer/loginViaFacebook',
    handler: function (request, reply) {
        Controllers.CustomerCtrl.customerLoginViaFacebook(request.payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
            }
        });
    },
    config: {
        description: 'Login Via Facebook',
        tags: ['api', 'customer'],
        validate: {
            payload: {
                facebookId: Joi.string().required(),
                deviceToken: Joi.string().required(),
                deviceType: Joi.string().required().valid([deviceType.IOS, deviceType.ANDROID])
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var customerVerifyOTP = {
    method: 'PUT',
    path: '/api/v1/customer/verifyOTP',
    handler: function (request, reply) {
        const userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerCtrl.verifyOTP(request.payload, userData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.VERIFY_COMPLETE, data));
            }
        });
    },
    config: {
        description: 'Verify OTP',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                OTPCode: Joi.string().length(4).required()
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

var customerResendOTP = {
    method: 'PUT',
    path: '/api/v1/customer/resendOTP',
    handler: function (request, reply) {
        var userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerCtrl.resendOTP(userData, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));
                //reply(err);
            } else {
                reply(UniversalFunctions.sendSuccess(null, data));
                //reply(null, data);
            }
        });
    },
    config: {
        description: 'Resend OTP for Customer',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const changePassword = {
    method: 'PUT',
    path: '/api/v1/customer/changePassword',
    handler: function (request, reply) {
        const userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.CustomerCtrl.changePassword(request.payload, userData, function (err, data) {
            if (!err) {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.PASSWORD_RESET, data));

            }
            else {
                reply(UniversalFunctions.sendError(err));

            }
        });
    },
    config: {
        description: 'Change Password',
        tags: ['api', 'customer'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                oldPassword: Joi.string().required().min(6),
                newPassword: Joi.string().required().min(6)
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

module.exports = [
    customerRegister,
    customerLogin,
    customerLogout,
    customerLoginViaFacebook,
    customerVerifyOTP,
    customerResendOTP,
    changePassword
];