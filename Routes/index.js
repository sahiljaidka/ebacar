/**
 * Created by SONY on 3/10/16.
 */

const CustomerRoute = require('./CustomerRoute');
const DriverRoute = require('./DriverRoute');

const all = [].concat(CustomerRoute, DriverRoute);
module.exports = all;