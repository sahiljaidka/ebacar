/**
 * Created by SONY on 5/29/2016.
 */
'use strict';

const uuid = require('node-uuid');
const Joi = require('joi');
const Controllers = require('../Controllers');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const Config = require('../Config');

const deviceType = Config.AppConstants.DATABASE.DEVICETYPE;

const driverRegisterStep1 = {
    method: 'POST',
    path: '/api/driver/registerStep1',
    handler: function (request, reply) {
        let payload = request.payload;
        Controllers.DriverCtrl.driverRegisterStep1(payload, function (err, data) {
            if (err) {
                reply(UniversalFunctions.sendError(err));

            } else {
                reply(UniversalFunctions.sendSuccess(Config.AppConstants.STATUS_MSG.SUCCESS.CREATED, data)).code(201);

            }
        });
    },
    config: {
        description: 'Driver Register Step 1',
        tags: ['api', 'driver'],
        /*      payload: {
         maxBytes: 2000000,
         output: 'file',
         parse: true
         },*/
        validate: {
            payload: {
                firstName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                lastName: Joi.string().regex(/^[a-zA-Z ]+$/).min(2).required(),
                email: Joi.string().required().email(),
                password: Joi.string().min(6).required(),
                countryCode: Joi.string().required(),
                mobileNumber: Joi.string().min(5).regex(/^[0-9]+$/).required(),
                deviceToken: Joi.string().required(),
                deviceType: Joi.string().required().valid([deviceType.IOS, deviceType.ANDROID])

                /* profilePic: Joi.any()
                 .meta({swaggerType: 'file'})
                 .optional()
                 .description('image file')*/
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                //payloadType: 'form',
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const driverRegisterStep2 = {
    method: 'PUT',
    path: '/api/driver/registerStep2',
    handler: function (request, reply) {
        const userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.DriverCtrl.driverRegisterStep2(request.payload, userData, function (err, data) {
            if (!err) {
                reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Driver Register Step 2',
        tags: ['api', 'driver'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                gender: Joi.string().optional().allow(''),
                address: Joi.string().required(),
                city: Joi.string().required(),
                state: Joi.string().required(),
                primaryZipCode: Joi.string().required(),
                additionalZipCodes: Joi.array().items(Joi.string()).required(),
                primaryHomeZone: Joi.string().required(),
                secondaryHomeZone: Joi.string().required(),
                companyCode: Joi.string().optional().allow('')
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

const driverAddVehicle = {
    method: 'POST',
    path: '/api/driver/addVehicle',
    handler: function (request, reply) {
        const userData = request.auth && request.auth.credentials && request.auth.credentials.userData || null;
        Controllers.DriverCtrl.addVehicle(request.payload, userData, (err, data) => {
            if (!err) {
                reply(UniversalFunctions.sendSuccess(null, data));
            }
            else {
                reply(UniversalFunctions.sendError(err));
            }
        });
    },
    config: {
        description: 'Driver Add Vehicle',
        tags: ['api', 'driver'],
        auth: 'UserAuth',
        validate: {
            headers: UniversalFunctions.authorizationHeaderObj,
            payload: {
                yearOfPurchase: Joi.number().required(),
                vehicleMake: Joi.string().required(),
                doorCount: Joi.number().required(),
                carColor: Joi.string().required(),
                seatCount: Joi.number().required(),
                stateRegTag: Joi.boolean().required(),
                smokingFlag: Joi.boolean().required()

                //TODO: Add picture
            },
            failAction: UniversalFunctions.failActionFunction
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: Config.AppConstants.swaggerDefaultResponseMessages
            }
        }
    }
};

module.exports = [
    driverRegisterStep1,
    driverRegisterStep2,
    driverAddVehicle
];